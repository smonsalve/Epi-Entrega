#!/usr/bin/env ruby

require 'roo'
require_relative 'epi_year'
require 'easystats'
require 'matrix'
require 'easystats'

years = []
promedioSemana = []
desviacion = []
ic95_superior = []
ic95_inferior = []

#puts "Leyendo base de datos... "
xlsx = Roo::Excelx.new("../input/Entradas.xlsx")
hoja1 = xlsx.sheet("input")

(2..hoja1.last_row).each do |r|
  fila = hoja1.row(r)
  e =  EpiYear.new(fila[0], fila[54])
  (1..52).each { |a| e.semanas << fila[a].to_i }
  years << e
  #puts e.poblacion
end

a = []
years.each do |y|
   a << y.semanas
end

b = Matrix.rows(a)

((0)..(b.column_count-1)).each do |col|
 avrg = b.column(col).to_a.average 
#   stdv = b.column(col).to_a.standard_deviation 
   promedioSemana << avrg
#   desviacion << stdv
#   ic95_superior << avrg + (2.02*(stdv/Math.sqrt(5)))
#   ic95_inferior << avrg - (2.02*(stdv/Math.sqrt(5)))
 end

#puts("Promedio de casos por semana")
#puts promedioSemana


# #__________________________________________________

# Año 
# Casos por Semana
# PoblacionTotal

# poblacionParaAño

# Promedio de Casos por semana  


 FactorCorreccion = PoblacionAño/promedioPoblacionAñosAnteriores
# º
# PromedioEsperado = PromedioSemana*FactorCorreccion
# PromedioMovil = PromedioEsperado i + (promedio Esperado i+1)%52 / 2 # ultimo con el primero
# error = (PromedioEsperado - PromedioMovil)^2
# error = suma de todos los errores / 52
# Desviacion = sqrt(error)
# 2D = 2*Desviacion 
# LimiteInferior = PromedioMovil - 2D # para cada Semana
# LimiteSuperior = PromedioMovil + 2D # para cada Semana


# Graficar

# LimiteInferior 
# PromedioMovil
# LimiteSuperior
# CasosReales


