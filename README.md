# Epi

El presente repositorio tiene como objetivo contener el código desarrollado
para el proyecto de predicción de Dengue del Departamento Matemática Aplicada
de la Universidad EAFIT

Para una versión funcional del Software visite: 

http://dengue.heroku.com

para una versión Web de esta Entrega visite: 

https://gitlab.com/smonsalve/Epi-Entrega

Para acceder a una versión Web de Este repositorio de la plataforma online Visite: 

https://github.com/smonsalve/Epi


# Información del Autor

* Sergio Andrés Monsalve Castañeda
* sergio@monsalve.xyz

# Contenido

## Aux

Información y Documentos Auxiliares, Fotos, logos y demas.

## doc: Documentación

Documentación general del Proyecto


## informes

Informes de Avances Semanales

## input

Bases de Datos de Entrada al sistema para pruebas unitarias y funcionales

## ODE 

Libreria para resolver ecuaciones diferenciales de primer orden.
Ejemplos y pruebas


## Plataforma

Código fuente de la plataforma online en Ruby on Rails

## Requisitos

Alisis de Requisitos, requerimientos por parte de los usuarios del sistema.

## src

Código Fuente



® TODOS LOS DERECHOS RESERVADOS