require 'test_helper'

class GeoPositionsControllerTest < ActionController::TestCase
  setup do
    @geo_position = geo_positions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:geo_positions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create geo_position" do
    assert_difference('GeoPosition.count') do
      post :create, geo_position: { lat: @geo_position.lat, lng: @geo_position.lng, zone_id: @geo_position.zone_id }
    end

    assert_redirected_to geo_position_path(assigns(:geo_position))
  end

  test "should show geo_position" do
    get :show, id: @geo_position
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @geo_position
    assert_response :success
  end

  test "should update geo_position" do
    patch :update, id: @geo_position, geo_position: { lat: @geo_position.lat, lng: @geo_position.lng, zone_id: @geo_position.zone_id }
    assert_redirected_to geo_position_path(assigns(:geo_position))
  end

  test "should destroy geo_position" do
    assert_difference('GeoPosition.count', -1) do
      delete :destroy, id: @geo_position
    end

    assert_redirected_to geo_positions_path
  end
end
