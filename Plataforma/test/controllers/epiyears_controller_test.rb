require 'test_helper'

class EpiyearsControllerTest < ActionController::TestCase
  setup do
    @epiyear = epiyears(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:epiyears)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create epiyear" do
    assert_difference('Epiyear.count') do
      post :create, epiyear: { poblacion: @epiyear.poblacion, s10: @epiyear.s10, s11: @epiyear.s11, s12: @epiyear.s12, s13: @epiyear.s13, s14: @epiyear.s14, s15: @epiyear.s15, s16: @epiyear.s16, s17: @epiyear.s17, s18: @epiyear.s18, s19: @epiyear.s19, s1: @epiyear.s1, s20: @epiyear.s20, s21: @epiyear.s21, s22: @epiyear.s22, s23: @epiyear.s23, s24: @epiyear.s24, s25: @epiyear.s25, s26: @epiyear.s26, s27: @epiyear.s27, s28: @epiyear.s28, s29: @epiyear.s29, s2: @epiyear.s2, s30: @epiyear.s30, s31: @epiyear.s31, s32: @epiyear.s32, s33: @epiyear.s33, s34: @epiyear.s34, s35: @epiyear.s35, s36: @epiyear.s36, s37: @epiyear.s37, s38: @epiyear.s38, s39: @epiyear.s39, s3: @epiyear.s3, s40: @epiyear.s40, s41: @epiyear.s41, s42: @epiyear.s42, s43: @epiyear.s43, s44: @epiyear.s44, s45: @epiyear.s45, s46: @epiyear.s46, s47: @epiyear.s47, s48: @epiyear.s48, s49: @epiyear.s49, s4: @epiyear.s4, s50: @epiyear.s50, s51: @epiyear.s51, s52: @epiyear.s52, s5: @epiyear.s5, s6: @epiyear.s6, s7: @epiyear.s7, s8: @epiyear.s8, s9: @epiyear.s9, year: @epiyear.year }
    end

    assert_redirected_to epiyear_path(assigns(:epiyear))
  end

  test "should show epiyear" do
    get :show, id: @epiyear
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @epiyear
    assert_response :success
  end

  test "should update epiyear" do
    patch :update, id: @epiyear, epiyear: { poblacion: @epiyear.poblacion, s10: @epiyear.s10, s11: @epiyear.s11, s12: @epiyear.s12, s13: @epiyear.s13, s14: @epiyear.s14, s15: @epiyear.s15, s16: @epiyear.s16, s17: @epiyear.s17, s18: @epiyear.s18, s19: @epiyear.s19, s1: @epiyear.s1, s20: @epiyear.s20, s21: @epiyear.s21, s22: @epiyear.s22, s23: @epiyear.s23, s24: @epiyear.s24, s25: @epiyear.s25, s26: @epiyear.s26, s27: @epiyear.s27, s28: @epiyear.s28, s29: @epiyear.s29, s2: @epiyear.s2, s30: @epiyear.s30, s31: @epiyear.s31, s32: @epiyear.s32, s33: @epiyear.s33, s34: @epiyear.s34, s35: @epiyear.s35, s36: @epiyear.s36, s37: @epiyear.s37, s38: @epiyear.s38, s39: @epiyear.s39, s3: @epiyear.s3, s40: @epiyear.s40, s41: @epiyear.s41, s42: @epiyear.s42, s43: @epiyear.s43, s44: @epiyear.s44, s45: @epiyear.s45, s46: @epiyear.s46, s47: @epiyear.s47, s48: @epiyear.s48, s49: @epiyear.s49, s4: @epiyear.s4, s50: @epiyear.s50, s51: @epiyear.s51, s52: @epiyear.s52, s5: @epiyear.s5, s6: @epiyear.s6, s7: @epiyear.s7, s8: @epiyear.s8, s9: @epiyear.s9, year: @epiyear.year }
    assert_redirected_to epiyear_path(assigns(:epiyear))
  end

  test "should destroy epiyear" do
    assert_difference('Epiyear.count', -1) do
      delete :destroy, id: @epiyear
    end

    assert_redirected_to epiyears_path
  end
end
