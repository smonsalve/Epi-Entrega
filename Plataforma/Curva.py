#! /usr/bin/python

import sys
import odespy, numpy
import base64
from math import sin, pi, sqrt
import matplotlib.pyplot as plt

# Huevos
# Larvas
# Pupas
# Mosquitos hembras suceptibles adultos	
# Mosquitos hembras expuestas
# Mosquitos hembras infecciosos
# Humanos suceptibles
# Humanos Expuestos
# Humanos Infecciosos
# Humanos Recuperados

E0		= int(sys.argv[1])
L0		= int(sys.argv[2])
P0		= int(sys.argv[3])
Ms0		= int(sys.argv[4])
Me0		= int(sys.argv[5])
Mi0		= int(sys.argv[6])
Hs0		= int(sys.argv[7])
He0		= int(sys.argv[8])
Hi0		= int(sys.argv[9])
Hr0 	= int(sys.argv[10])

# 0 0,0,100000,1340,0,330590,0,8,0
# 1680000,1570000,1600000,1700000,1340,0,330590,0,8,0
# 15409,13279,20707,1700000,1340,0,330590,0,8,0

delta	=	240
C		=	6002
gamma_e	=	0.0395
mu_e	=	0.0954
gamma_l	=	1.08
mu_l	=	1.07
gamma_p	=	2.09
mu_p	=	0.486
F		=	0.374
beta_m	=	0.0449
mu_m	=	0.274
theta_m	=	0.314
mu_h	=	0.000388
beta_h	=	0.0795
theta_h	=	0.379
gamma_h	=	0.398
c_e		=	0
c_l		=	0
c_m		=	0
c_p		=	0
v 		=	0

def f(u, t):
	E,L,P,Ms,Me,Mi,Hs,He,Hi,Hr = u

	#M = Ms + Me + Mi
	#H = Hs + He + Hi + Hr

	E1 	= (((delta * (1 - (E / C))) * (Ms + Me + Mi)) - ((gamma_e + mu_e + c_e) * E))
	L1 	= ((gamma_e * E) - ((gamma_l + mu_l + c_l) * L))
	P1 	= ((gamma_l * L) - ((gamma_p + mu_p + c_p) * P))
	Ms1 	= ((F * gamma_p * P) - ((beta_m * Hi * Ms) / (Hs + He + Hi + Hr)) - ((mu_m + c_m) * Ms))
	Me1  = (((beta_m * Hi * Ms) / (Hs + He + Hi + Hr)) - ((theta_m + mu_m + c_m) * Me))
	Mi1  = ((theta_m * Me) - ((mu_m + c_m) * Mi))
	Hs1  = ((mu_h * (He + Hi + Hr)) - ((beta_h * Mi * Hs)/(Ms + Me + Mi))  - (v * Hs))
	He1  = (((beta_h * Mi * Hs) / (Ms + Me + Mi)) - ((theta_h + mu_h) * He))
	Hi1 	= ((theta_h * He) - ((gamma_h + mu_h) * Hi))
	Hr1  = ((gamma_h * Hi) - (mu_h * Hr) + (v * Hs)) 

	if (	E1	 < 0) 	: E1 	= 0
	# if (	L1	 < 0) 	: L1 	= 0
	# if (	P1	 < 0) 	: P1 	= 0
	# if (	Ms1	 < 0) 	: Ms1 	= 0
	# if (	Me1	 < 0) 	: Me1 	= 0
	# if (	Mi1	 < 0) 	: Mi1 	= 0
	# if (	Hs1	 < 0) 	: Hs1 	= 0
	# if (	He1	 < 0) 	: He1 	= 0
	# if (	Hi1	 < 0) 	: Hi1 	= 0
	# if (	Hr1	 < 0) 	: Hr1 	= 0

	#print E1,L1,P1,Ms1,Me1,Mi1,Hs1,He1,Hi1,Hr1

	return [E1, L1, P1, Ms1, Me1, Mi1, Hs1, He1, Hi1, Hr1]
	

solver = odespy.RK4(f)
r = ([E0,L0,P0,Ms0,Me0,Mi0,Hs0,He0,Hi0,Hr0])
solver.set_initial_condition(r)
rango = numpy.linspace(0,100,10000)
u, t = solver.solve(rango)

#print u

# U es una matriz de puntos 

E  = u[:,0]   	# Se extrae el Valor de E de la Matriz y se crea un arreglo solo con los valores de E
L  = u[:,1]		# Se extrae el Valor de L  de la Matriz y se crea un arreglo solo con los valores de L 
P  = u[:,2]	   	# Se extrae el Valor de P  de la Matriz y se crea un arreglo solo con los valores de P 		
Ms = u[:,3]	   	# Se extrae el Valor de Ms de la Matriz y se crea un arreglo solo con los valores de Ms
Me = u[:,4]	   	# Se extrae el Valor de Me de la Matriz y se crea un arreglo solo con los valores de Me
Mi = u[:,5]	   	# Se extrae el Valor de Mi de la Matriz y se crea un arreglo solo con los valores de Mi
Hs = u[:,6]	   	# Se extrae el Valor de Hs de la Matriz y se crea un arreglo solo con los valores de Hs
He = u[:,7]	   	# Se extrae el Valor de He de la Matriz y se crea un arreglo solo con los valores de He
Hi = u[:,8]	   	# Se extrae el Valor de Hi de la Matriz y se crea un arreglo solo con los valores de Hi
Hr = u[:,9]	   	# Se extrae el Valor de Hr de la Matriz y se crea un arreglo solo con los valores de Hr

fig = plt.figure()
fig.suptitle('Curva de Prevalencia', fontsize=14, fontweight='bold')
ax = fig.add_subplot(341)
ax.plot(t,E, 'r-')
ax = fig.add_subplot(342)
ax.plot(t,L, 'b-')
ax = fig.add_subplot(343)
ax.plot(t,P, 'y-')
ax = fig.add_subplot(344)
ax.plot(t,Ms, 'r-')
ax = fig.add_subplot(345)
ax.plot(t,Me, 'b-')
ax = fig.add_subplot(346)
ax.plot(t,Mi, 'y-')
ax = fig.add_subplot(347)de
ax.plot(t,Hs, 'r-')
ax = fig.add_subplot(348)
ax.plot(t,He, 'b-')
ax = fig.add_subplot(349)
ax.plot(t,Hi, 'y-')
ax = fig.add_subplot(3,4,10)
ax.plot(t,Hr, 'r-')

#plt.show()
plt.savefig('tmp.png')
with open("tmp.png", "rb") as image_file:
    encoded_string = base64.b64encode(image_file.read())
print encoded_string