class GeoPositionsController < ApplicationController
  before_action :set_geo_position, only: [:show, :edit, :update, :destroy]

  # GET /geo_positions
  # GET /geo_positions.json
  def index
    @geo_positions = GeoPosition.all
  end

  # GET /geo_positions/1
  # GET /geo_positions/1.json
  def show
  end

  # GET /geo_positions/new
  def new
    @geo_position = GeoPosition.new
  end

  # GET /geo_positions/1/edit
  def edit
  end

  # POST /geo_positions
  # POST /geo_positions.json
  def create
    @geo_position = GeoPosition.new(geo_position_params)

    respond_to do |format|
      if @geo_position.save
        format.html { redirect_to @geo_position, notice: 'Geo position was successfully created.' }
        format.json { render :show, status: :created, location: @geo_position }
      else
        format.html { render :new }
        format.json { render json: @geo_position.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /geo_positions/1
  # PATCH/PUT /geo_positions/1.json
  def update
    respond_to do |format|
      if @geo_position.update(geo_position_params)
        format.html { redirect_to @geo_position, notice: 'Geo position was successfully updated.' }
        format.json { render :show, status: :ok, location: @geo_position }
      else
        format.html { render :edit }
        format.json { render json: @geo_position.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /geo_positions/1
  # DELETE /geo_positions/1.json
  def destroy
    @geo_position.destroy
    respond_to do |format|
      format.html { redirect_to geo_positions_url, notice: 'Geo position was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_geo_position
      @geo_position = GeoPosition.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def geo_position_params
      params.require(:geo_position).permit(:lat, :lng, :zone_id)
    end
end
