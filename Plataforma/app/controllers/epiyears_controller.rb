class EpiyearsController < ApplicationController
  before_action :set_epiyear, only: [:show, :edit, :update, :destroy]


  def import
    Epiyear.import(params[:file])
    redirect_to epiyears_url, notice: "Datos Importados."
  end

  # GET /epiyears
  # GET /epiyears.json
  def index
    @epiyears = Epiyear.all
  end

  # GET /epiyears/1
  # GET /epiyears/1.json
  def show
  end

  # GET /epiyears/new
  def new
    @epiyear = Epiyear.new
  end

  # GET /epiyears/1/edit
  def edit
  end

  # POST /epiyears
  # POST /epiyears.json
  def create
    @epiyear = Epiyear.new(epiyear_params)

    respond_to do |format|
      if @epiyear.save
        format.html { redirect_to @epiyear, notice: 'Epiyear was successfully created.' }
        format.json { render :show, status: :created, location: @epiyear }
      else
        format.html { render :new }
        format.json { render json: @epiyear.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /epiyears/1
  # PATCH/PUT /epiyears/1.json
  def update
    respond_to do |format|
      if @epiyear.update(epiyear_params)
        format.html { redirect_to @epiyear, notice: 'Epiyear was successfully updated.' }
        format.json { render :show, status: :ok, location: @epiyear }
      else
        format.html { render :edit }
        format.json { render json: @epiyear.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /epiyears/1
  # DELETE /epiyears/1.json
  def destroy
    @epiyear.destroy
    respond_to do |format|
      format.html { redirect_to epiyears_url, notice: 'Epiyear was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_epiyear
      @epiyear = Epiyear.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def epiyear_params
      params.require(:epiyear).permit(:year, :s1, :s2, :s3, :s4, :s5, :s6, :s7, :s8, :s9, :s10, :s11, :s12, :s13, :s14, :s15, :s16, :s17, :s18, :s19, :s20, :s21, :s22, :s23, :s24, :s25, :s26, :s27, :s28, :s29, :s30, :s31, :s32, :s33, :s34, :s35, :s36, :s37, :s38, :s39, :s40, :s41, :s42, :s43, :s44, :s45, :s46, :s47, :s48, :s49, :s50, :s51, :s52, :poblacion)
    end
end
