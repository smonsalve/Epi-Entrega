class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy]

  # GET /events
  # GET /events.json



  def import
    Event.import(params[:file])
    redirect_to events_url, notice: "Casos importados."
  end

  def index
    @events = Event.all
    #@zones = Zone.includes(:geoPositions).select('zones.*,geo_positions.lat')
    @zones = Zone.all
    #@geo_positions = GeoPosition.all

    respond_to do |format|
      format.html
      format.csv { send_data @events.to_csv }
      format.xls { send_data @events.to_csv(col_sep: "\t") }
    end
  end


  # GET /events/1
  # GET /events/1.json
  def show
  end

  # GET /events/new
  def new
    @event = Event.new
  end

  # GET /events/1/edit
  def edit
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(event_params)
    findLatLng(@event)
    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    respond_to do |format|

      if @event.update(event_params)
        findLatLng(@event)
        @event.save
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:address, :lat, :lng)
    end

    def findLatLng(event)
      current_address = event.address
      current_address = current_address.gsub " " ,"+"

      uri = URI.parse("https://maps.googleapis.com/maps/api/geocode/json?address=#{current_address}&key=AIzaSyBLN-x8gT8AFbvs3vyF0FWY50NK1uv3_b4")
      #uri = URI.parse("http://google.com/")

      # Shortcut
    response = Net::HTTP.get_response(uri)

    jsonResult = JSON.parse response.body

    #jsonResult.results[0].geometry.location.lat
    Net::HTTP.get_print(uri)
    Rails.logger.debug { "--------------------------- -----------response lat #{jsonResult['results'][0]['geometry']['location']['lat']} lng #{jsonResult['results'][0]['geometry']['location']['lng']}"  }

    event.lat = jsonResult['results'][0]['geometry']['location']['lat']
    event.lng = jsonResult['results'][0]['geometry']['location']['lng']
    end
end
