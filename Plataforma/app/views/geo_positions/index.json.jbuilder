json.array!(@geo_positions) do |geo_position|
  json.extract! geo_position, :id, :lat, :lng, :zone_id
  json.url geo_position_url(geo_position, format: :json)
end
