json.array!(@faqs) do |faq|
  json.extract! faq, :id, :pregunta, :respuesta
  json.url faq_url(faq, format: :json)
end
