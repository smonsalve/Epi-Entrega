json.array!(@events) do |event|
  json.extract! event, :id, :address, :lat, :lng
  json.url event_url(event, format: :json)
end
