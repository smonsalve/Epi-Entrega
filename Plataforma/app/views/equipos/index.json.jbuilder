json.array!(@equipos) do |equipo|
  json.extract! equipo, :id, :foto, :nombre, :cargo, :fechaInicio, :fechaFin, :perfil, :responsabilidades
  json.url equipo_url(equipo, format: :json)
end
