class Role < ActiveRecord::Base
	has_many :permissions
 	has_many :users, :through => :permissions

 	def admin?(role)
 		role.eql?("admin")
 	end
end
