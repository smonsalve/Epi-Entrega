class Zone < ActiveRecord::Base
  has_many :geoPositions
  accepts_nested_attributes_for :geoPositions,allow_destroy: true

  def self.import(file)
    spreadsheet = open_spreadsheet(file)
    spreadsheet.each_with_pagename do |name, sheet|
      header = sheet.row(1)
      zone 
      (2..spreadsheet.last_row).each do |i|
        row = Hash[[header, spreadsheet.row(i)].transpose]
        geoPositions = find_by_id(row["id"]) || new
        zone.attributes = row.to_hash.slice(*Zone.attribute_names(), name)
        zone.save!
      end
    end
  end

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
      #when ".csv" then Csv.new(file.path, nil, :ignore)
      when ".xls" then Roo::Excel.new(file.path)
      when ".xlsx" then Roo::Excelx.new(file.path)
      else raise "Unknown file type: #{file.original_filename}"
    end
  end
end
