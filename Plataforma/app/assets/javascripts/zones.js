//# Place all the behaviors and hooks related to the matching controller here.
//# All this logic will automatically be available in application.js.
//# You can use CoffeeScript in this file: http://coffeescript.org/
/*$ ->
  $('.remove_link_class').click = ()->
    $(link).previous("input[type=hidden]").value = 1
    $(link).up(".fields").hide*/

var ready;
ready = function() {

  $('.remove_link_class').click(function(event) {
    $(this).prev("input[type=hidden]").val("true");
    $(this).parent(".fields").hide();
    event.preventDefault();
  });

  $('.add_fields').click(function(event) {
    var time = new Date().getTime()
    var regexp = new RegExp($(this).data('id'), 'g')
    $(this).before($(this).data('fields').replace(regexp, time))
    event.preventDefault()

  });
};

$( document ).ready(function() {

  ready();



  function add_fields(link, association, content) {
    var new_id = new Date().getTime();
    var regexp = new RegExp("new_" + association, "g")
    $(link).parent().before(content.replace(regexp, new_id));
  }
});
$(document).on('page:load', ready);
