class CreateEquipos < ActiveRecord::Migration
  def change
    create_table :equipos do |t|
      t.string :foto
      t.string :nombre
      t.string :cargo
      t.date :fechaInicio
      t.date :fechaFin
      t.text :perfil
      t.text :responsabilidades

      t.timestamps null: false
    end
  end
end
