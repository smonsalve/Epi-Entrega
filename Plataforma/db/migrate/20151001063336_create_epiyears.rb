class CreateEpiyears < ActiveRecord::Migration
  def change
    create_table :epiyears do |t|
      t.integer :year
      t.integer :s1
      t.integer :s2
      t.integer :s3
      t.integer :s4
      t.integer :s5
      t.integer :s6
      t.integer :s7
      t.integer :s8
      t.integer :s9
      t.integer :s10
      t.integer :s11
      t.integer :s12
      t.integer :s13
      t.integer :s14
      t.integer :s15
      t.integer :s16
      t.integer :s17
      t.integer :s18
      t.integer :s19
      t.integer :s20
      t.integer :s21
      t.integer :s22
      t.integer :s23
      t.integer :s24
      t.integer :s25
      t.integer :s26
      t.integer :s27
      t.integer :s28
      t.integer :s29
      t.integer :s30
      t.integer :s31
      t.integer :s32
      t.integer :s33
      t.integer :s34
      t.integer :s35
      t.integer :s36
      t.integer :s37
      t.integer :s38
      t.integer :s39
      t.integer :s40
      t.integer :s41
      t.integer :s42
      t.integer :s43
      t.integer :s44
      t.integer :s45
      t.integer :s46
      t.integer :s47
      t.integer :s48
      t.integer :s49
      t.integer :s50
      t.integer :s51
      t.integer :s52
      t.integer :poblacion

      t.timestamps null: false
    end
  end
end
