class CreateFaqs < ActiveRecord::Migration
  def change
    create_table :faqs do |t|
      t.string :pregunta
      t.text :respuesta

      t.timestamps null: false
    end
  end
end
