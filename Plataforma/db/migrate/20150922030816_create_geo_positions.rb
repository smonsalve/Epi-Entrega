class CreateGeoPositions < ActiveRecord::Migration
  def change
    create_table :geo_positions do |t|
      t.decimal :lat
      t.decimal :lng
      t.integer :zone_id

      t.timestamps null: false
    end
  end
end
