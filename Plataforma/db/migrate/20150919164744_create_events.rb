class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :address
      t.decimal :lat
      t.decimal :lng

      t.timestamps null: false
    end
  end
end
