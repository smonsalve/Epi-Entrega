class CreatePermissions < ActiveRecord::Migration
  def change
    create_table :permissions do |t|
      t.string :user_id
      t.string :role_id
      t.string :activity

      t.timestamps null: false
    end
  end
end
