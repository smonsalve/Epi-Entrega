class AddImageProcessedToEquipo < ActiveRecord::Migration
  def change
    add_column :equipos, :image_processed, :boolean
  end
end
