# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151001063336) do

  create_table "epiyears", force: :cascade do |t|
    t.integer  "year"
    t.integer  "s1"
    t.integer  "s2"
    t.integer  "s3"
    t.integer  "s4"
    t.integer  "s5"
    t.integer  "s6"
    t.integer  "s7"
    t.integer  "s8"
    t.integer  "s9"
    t.integer  "s10"
    t.integer  "s11"
    t.integer  "s12"
    t.integer  "s13"
    t.integer  "s14"
    t.integer  "s15"
    t.integer  "s16"
    t.integer  "s17"
    t.integer  "s18"
    t.integer  "s19"
    t.integer  "s20"
    t.integer  "s21"
    t.integer  "s22"
    t.integer  "s23"
    t.integer  "s24"
    t.integer  "s25"
    t.integer  "s26"
    t.integer  "s27"
    t.integer  "s28"
    t.integer  "s29"
    t.integer  "s30"
    t.integer  "s31"
    t.integer  "s32"
    t.integer  "s33"
    t.integer  "s34"
    t.integer  "s35"
    t.integer  "s36"
    t.integer  "s37"
    t.integer  "s38"
    t.integer  "s39"
    t.integer  "s40"
    t.integer  "s41"
    t.integer  "s42"
    t.integer  "s43"
    t.integer  "s44"
    t.integer  "s45"
    t.integer  "s46"
    t.integer  "s47"
    t.integer  "s48"
    t.integer  "s49"
    t.integer  "s50"
    t.integer  "s51"
    t.integer  "s52"
    t.integer  "poblacion"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "equipos", force: :cascade do |t|
    t.string   "foto"
    t.string   "nombre"
    t.string   "cargo"
    t.date     "fechaInicio"
    t.date     "fechaFin"
    t.text     "perfil"
    t.text     "responsabilidades"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.boolean  "image_processed"
  end

  create_table "events", force: :cascade do |t|
    t.string   "address"
    t.decimal  "lat"
    t.decimal  "lng"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "faqs", force: :cascade do |t|
    t.string   "pregunta"
    t.text     "respuesta"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "geo_positions", force: :cascade do |t|
    t.decimal  "lat"
    t.decimal  "lng"
    t.integer  "zone_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "permissions", force: :cascade do |t|
    t.string   "user_id"
    t.string   "role_id"
    t.string   "activity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "role"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

  create_table "zones", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
