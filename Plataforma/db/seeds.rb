Role.create!([
  {name: "Admin"},
  {name: "God"}
])	

# User.create!([
#   {email: "smonsal3@eafit.edu.co", encrypted_password: "$2a$10$WPSVMRsCRpcccZSM8RIZ7O4VYO0WYGtDSt3GQKvzJPpkSTP8luLzS", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 2, current_sign_in_at: "2015-08-12 14:37:27", last_sign_in_at: "2015-08-11 20:38:43", current_sign_in_ip: "::1", last_sign_in_ip: "::1", role: nil},
#   {email: "hola@hola.com", encrypted_password: "$2a$10$Ac4iHVJXL4HIsJ4GNQ8K7OpZBniVNeYX6R72TmdpFkdd9N.1UkOu6", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 0, current_sign_in_at: nil, last_sign_in_at: nil, current_sign_in_ip: nil, last_sign_in_ip: nil, role: nil}
# ])
Equipo.create!([
# Sergio Monsalve, Asistente de Investigación, 2015-06-01, 2015-10-08, Ingeniero de Sistemas Estudiante Maestria en Ingeniería Especialización en TIC's (Tecnologías de información y comunicación) para la Educación High Performance Computing, Desarrollador Web

	{
		nombre: "María Eugenia Puerta Yepes",
		#correo: "mpuerta@eafit.edu.co",
		foto: "MariaEugenia.jpg",
		perfil: "Profesora e investigadora del Departamento de Ciencias Matemáticas de la Universidad EAFIT desde 1997. Doctora en Ciencias Matemáticas de la Universidad Politécnica de Valencia, España y Matemática de la Universidad de Antioquia. Se ha desempeñado como jefe del Departamento de Ciencias Básicas (2003-2005) de la Universidad EAFIT. En la actualidad es la Directora del grupo de Investigación en Análisis Funcional y Aplicaciones(categoría A Colciencias). Ha publicado varios capítulos de libro en asuntos relacionados con Análisis Intervalo y Optimización y artículos en Análisis Funcional. Sus intereses académicos e investigativos en la actualidad son en Análisis Funcional, Optimización y Biomatemática.",
		cargo: "Co-investigadora en el proyecto y responsable de liderar la componente matemática del proyecto",
		responsabilidades:  "Diseño y ajuste  del Modelo Matemático de transmisión del dengue",
		#abstract: "",
		fechaInicio: "2013-10-08",
		fechaFin: "2015-10-08"
	},
	{
		nombre:"Sair Orieta Arboleda Sanchez",
		foto:"sair.jpg",
		perfil:"Profesora e investigadora del Instituto de Biología de la Universidad de Antioquia desde 2007. Bióloga, Magíster en Biología y Doctora en Biología de la Universidad de Antioquia. En la actualidad lidera la línea de investigación 'sistemas de información geográfica para el estudio de la eco-epidemiología de las enfermedades transmitidas por vectores', adscrita al grupo de Biología y Control de Enfermedades Infecciosas (BCEI, categoría A de Colciencias). Ha publicado varios artículos científicos y capítulos de libro relacionados con su tema de investigación.",
		#correo:sair.arboleda@udea.edu.co,
		cargo:"Docente Ocasional Universidad de Antioquia Seccional Oriente",
		responsabilidades:"Co-investigadora en el proyecto y responsable de liderar la componente biológica del proyecto, específicamente lo relacionado con la competencia y capacidad vectorial de Ae. aegypti.",
		fechaInicio:"2013-10-08",
		fechaFin:"2015-10-08"
		#abstract: 

	},
	{
		nombre: "Diana Paola Lizarralde B.",
		#correo: paola.lizarralde@gmail.com
		foto: "no tengo todavía.jpg",
		perfil: "Matemática de la Universidad Sergio Arboleda de Bogotá. Se ha desempeñado como docente catedrática en la Universidad Distrital Francisco José de Caldas en Bogotá (2009-2014). En la actualidad se encuentra cursando la maestría en Matemática Aplicada en la Universidad EAFIT. En la actualidad sus intereses académicos e investigativos son en EDO y Bio-matemática.",
		cargo: "Asistente de investigación.",
		responsabilidades: "Ayudar en la formulación de un modelo de transmisión de dengue, con base en la literatura y la información disponible.",
		#abstract : "Dengue is the most important viral disease transmitted by vectors in Colombia. At moment it is not treatment or vaccine available for its control and prevention; therefore, the only solution is exert control over mosquito population. In order to reduce its economical impact, it is important to focus control based on local characteristics of dengue epidemiology knowing the main factors involved in an epidemic. For that reason, we developed a mathematical model based in ordinary differential equations and using experimental data of mosquito populations from Bello (Antioquia, Colombia) to simulate the epidemic occurred in 2010. Results showed that biting rate, mortality rate of adult mosquitoes, and virus transmission probabilities are the most sensitive parameters to increase the number of dengue cases. Finally, we found that Basic Reproductive Number (R0) of these epidemic ranged between 1.2 − 1.6, with an infection force (Λ) of 0.061; meaning that is enough R0 values slightly above 1 to produce an important epidemic of dengue",
		fechaInicio: "2014-03-04",
		fechaFin: "2015-10-08"
	},
	{
		nombre: "Carlos Mario Vélez Sánchez",
		#correo: "cmvelez@eafit.edu.co",
		foto: "CarlosMarioVelez.jpg",
		perfil: "Profesor e investigador del Departamento de Ciencias Matemáticas de la Universidad EAFIT desde 1999. Doctor en Automática e Informática Industrial de la Universidad Politécnica de Valencia, España (2001). Físico (1988) y Master of Sciences en Física Matemática (1989) de la Universidad Estatal de Kishinev (exURSS). Ha sido jefe del pregrado de Ingeniería Matemática (2008-2011), Coordinador de la Maestría en Matemáticas Aplicadas (2002-2003), Presidente y Fundador de IEEE Control Systems Society Capítulo Colombia (2009-2011) y Presidente de la Asociación Colombiana de Automática ( 2002-2006, 1999-2000). Sus áreas de interés en la actualidad son los Sistemas Dinámicos, la Estimación de Parámetros, la Estimación del Estado y los Sistemas de Control, los cuales ha aplicado a Sistemas de Aeronaves no Tripuladas (UAS) Autónomas y el Dengue. Ha publicado varios artículos y ponencias nacionales e internacionales.",
		cargo: "Profesor de tiempo completo en la Universidad EAFIT. Coinvestigador",
		responsabilidades: "estimación de parámetros del modelo de propagación del dengue, análisis de incertidumbre y sensibilidad, y diseño de estrategias de control basadas en el modelo",
		#abstract: "Los modelos matemáticos de propagación del dengue incluyen parámetros supuestos u obtenidos experimentalmente, generalmente sin ningún análisis biológico de los rangos factibles. En el trabajo se aplica el método de optimización de mínimos cuadrados no lineal bajo la herramienta "Simulink Design Optimization" de Matlab para la estimación de 8 condiciones iniciales y 15 parámetros, partiendo de rangos biológicos y explicando las razones por las cuales los parámetros se salen significativamente de dichos rangos. Se presenta una metodología para hallar parámetros representativos, evitar mínimos locales y llegar a resultados más confiables. Además, se realiza un análisis de sensibilidad para identificar los parámetros más y menos significativos del modelo y un análisis de incertidumbre para estimar el efecto de los errores de los parámetros en los pronósticos dados. En el tema de control, se analiza el efecto de las acciones de control, como la fumigación, en los datos experimentales y el hipotético efecto de otras acciones de control (control mecánico, fumigación periódica, vacunación, etc.) a partir del modelo experimental obtenido, en el caso que se hubieran aplicado. Los resultados obtenidos muestran las ventajas del enfoque propuesto y su posible generalización a la propagación del dengue en otras regiones y a otras enfermedades.",
		fechaInicio: "2015-01-19",
		fechaFin: "2015-10-08"
	},
	{
		nombre: "Sergio Andrés Monsalve Castañeda",
  		foto: "Sergio.jpg",
		cargo: "Asistente de Investigación", 
		fechaInicio: "2015-06-01", 
		fechaFin: "2015-10-08", 
		perfil: "Ingeniero de Sistemas \r\nEstudiante Maestria en Ingeniería\r\nEspecialización en TIC's (Tecnologías de información y comunicación) para la Educación\r\nHigh Performance Computing", 
		responsabilidades: "Desarrollador Web"
		#image_processed: nil
	},
	{
		nombre: "Mayra",
  		foto: "Mayra.jpg",
		cargo: "Estudiante de Maestría", 
		fechaInicio: "2014-11-01", 
		fechaFin: "2015-10-08", 
		perfil: "",
		responsabilidades: "Encargada Mapas de riesgo"
		#image_processed: nil
	},
])